angular.module('zinzinApp')
    .directive( 'zinzinApp', [ '$http', '$sce', function ( $http, $sce ) {
    return {
        replace: true,
        restrict: "E",
        scope: {
            url: '@'
        }
    }
}]);