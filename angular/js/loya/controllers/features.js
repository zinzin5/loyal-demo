zinzinApp
    .controller('FeaturesCtrl', function ($scope) {
        $scope.datas = [{
            'name': 'Scan',
            'content': 'Amplify Global South countries crowdsourcing poverty, dedicated social innovation sanitation expanding community. Amplify Global South countries crowdsourcing poverty, dedicated social innovation sanitation expanding community.',
            'icon': 'fa-search'
        }, {
            'name': 'Stores',
            'content': 'Amplify Global South countries crowdsourcing poverty, dedicated social innovation sanitation expanding community. Amplify Global South countries crowdsourcing poverty, dedicated social innovation sanitation expanding community.',
            'icon': 'fa-shopping-cart'
        }, {
            'name': 'Deals',
            'content': 'Amplify Global South countries crowdsourcing poverty, dedicated social innovation sanitation expanding community. Amplify Global South countries crowdsourcing poverty, dedicated social innovation sanitation expanding community.',
            'icon': 'fa-tag'
        }, {
            'name': 'Places',
            'content': 'Amplify Global South countries crowdsourcing poverty, dedicated social innovation sanitation expanding community. Amplify Global South countries crowdsourcing poverty, dedicated social innovation sanitation expanding community.',
            'icon': 'fa-map-marker'
        }];
    });