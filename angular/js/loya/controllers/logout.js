zinzinApp
    .controller('LogoutCtrl', function($http, $location , toastr, APIService, $localStorage){
        if (!APIService.isAuthenticated()) { return; }
        delete $localStorage.userData;
        toastr.info('You have been logged out');
        $location.path('/');
    });