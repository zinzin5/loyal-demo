angular.module('zinzinApp')
    .controller('LoginCtrl', function($http, $scope, $location , toastr, APIService, $localStorage) {
        $scope.$storage = $localStorage;
        $scope.login = function() {

            var requestData = new FormData();
            angular.forEach($scope.user, function(value, key) {
                requestData.append(key, value);
            });

            var req = {
                method: 'POST',
                url: APIService.loginUrl,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity,
                data: requestData
            }
            $http(req).success(function(data, status, headers, config){
                if (data.error == 0){
                    $localStorage.userData = data.data;
                    toastr.success('You have successfully signed in');
                    $location.path('/');
                }
                else{
                    $scope.reset();
                    toastr.error(data.message);
                }
            }).error(function(data){
                $scope.reset();
                $scope.message = data.message;
                toastr.error(data.message, data.status);
            });
        };

        $scope.reset = function(){
          $scope.user.c_password = '';
          $scope.user.confirmPassword = '';
        };
    });