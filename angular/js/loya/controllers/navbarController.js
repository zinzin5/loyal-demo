angular.module('zinzinApp')
    .controller('NavCtrl', function($scope, $log, $anchorScroll, $location) {
        $scope.gotoAnchor = function(div) {
            console.log(div);
            console.log($location.hash());
      var newHash = div;
      if ($location.hash() !== newHash) {
        // set the $location.hash to `newHash` and
        // $anchorScroll will automatically scroll to it
        $location.hash(newHash);
      } else {
        // call $anchorScroll() explicitly,
        // since $location.hash hasn't changed
        $anchorScroll();
      }
    };

        $scope.listMenus = [{
            'name': 'FEATURES',
            'hasSub': false,
            'link': '#features',
            'icon' : 'fa-home',
            'id' : 'features'
        }, {
            'name': 'THE CONCEPT',
            'hasSub': false,
            'subMenu': [{
                'name': 'For the week',
                'link': '/popular/week'
            }, {
                'name': 'For the month',
                'link': '/popular/month'
            }, {
                'name': 'For the Year',
                'link': '/popular/year'
            }, {
                'name': 'All time',
                'link': '/popular/all'
            }],
            'link': '#concept',
            'icon' : 'fa-star',
            'id' : 'concept'
        }, {
            'name': 'HOW IT WORKS',
            'hasSub': false,
            'subMenu': [{
                'name': 'GIFs',
                'link': '/cat/gifs'
            }, {
                'name': 'Videos',
                'link': '/cat/videos'
            }, {
                'name': 'Images',
                'link': '/cat/images'
            }],
            'link': '#howtowork',
            'icon' : 'fa-folder-open',
            'id' : 'howtowork'
        }, {
            'name': 'REQUEST INFO',
            'hasSub': false,
            'subMenu': [{
                'name': 'Add Pages',
                'link': '/add_pages'
            }],
            'link': '#request',
            'icon' : 'fa-file-text',
            'id' : 'request'
        },{
            'name': 'CONTACT US',
            'hasSub': false,
            'subMenu': [{
                'name': 'Add Pages',
                'link': '/add_pages'
            }],
            'link': '#contact',
            'icon' : 'fa-file-text',
            'id' : 'contact'
        }];
    });