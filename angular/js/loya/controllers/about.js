zinzinApp
    .controller('AboutUsCtrl', function ($scope) {
        $scope.datas = [{
            'name': 'Who we are',
            'content': 'Amplify Global South countries crowdsourcing poverty, dedicated social innovation sanitation expanding community. Amplify Global South countries crowdsourcing poverty, dedicated social innovation sanitation expanding community.',
            'icon': 'fa-search'
        }, {
            'name': 'What we do',
            'content': 'Amplify Global South countries crowdsourcing poverty, dedicated social innovation sanitation expanding community.',
            'icon': 'fa-shopping-cart'
        }, {
            'name': 'Out mission',
            'content': 'Amplify Global South countries crowdsourcing poverty, dedicated social innovation sanitation expanding community. Amplify Global South countries crowdsourcing poverty, dedicated social innovation sanitation expanding community.',
            'icon': 'fa-tag'
        }, {
            'name': 'Our vision',
            'content': 'Amplify Global South countries crowdsourcing poverty, dedicated social innovation sanitation expanding community. Amplify Global South countries crowdsourcing poverty, dedicated social innovation sanitation expanding community.',
            'icon': 'fa-map-marker'
        }];
    });