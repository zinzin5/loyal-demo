zinzinApp
    .controller('SignupCtrl', function ($scope, $http, $location, toastr, APIService, $localStorage, Upload, $timeout, fileReader) {
        $scope.getFile = function () {
            $scope.progress = 0;
            fileReader.readAsDataUrl($scope.picFile, $scope)
                .then(function(result) {
                    $scope.imageSrc = result;
                });
        };
        $scope.signup = function (file) {
            var requestData = new FormData();
            angular.forEach($scope.user, function (value, key) {
                requestData.append(key, value);
            });
            if(file)
                requestData.append('c_avatar', file);
            var req = {
                method: 'POST',
                url: APIService.signupUrl,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity,
                data: requestData
            }
            file.upload = Upload.http(req);

            file.upload.then(function (data) {
                $timeout(function () {
                    if (data.error == 0) {
                        $localStorage.userData = data.data;
                        toastr.success('You have successfully signed in');
                        $location.path('/');
                    }
                    else {
                        toastr.error(data.message);
                    }
                });
            }, function (data) {
                $scope.message = data.message;
                toastr.error(data.message, data.status);
            });

            file.upload.progress(function (evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });

            //$http(req).success(function (data, status, headers, config) {
            //    if (data.error == 0) {
            //        $localStorage.userData = data.data;
            //        toastr.success('You have successfully signed in');
            //        $location.path('/');
            //    }
            //    else {
            //        toastr.error(data.message);
            //    }
            //}).error(function (data) {
            //    $scope.message = data.message;
            //    toastr.error(data.message, data.status);
            //});
        };
    });

zinzinApp.directive("ngFileSelect",function(){

    return {
        link: function($scope,el){

            el.bind("change", function(e){
                $scope.getFile();
            })

        }

    }


})