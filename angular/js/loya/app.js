var zinzinApp = angular.module('zinzinApp', ['ui.router']);
zinzinApp
    .config(['$stateProvider','$urlRouterProvider', '$locationProvider',function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'elements/home.html'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'elements/login.html',
                controller: 'LoginCtrl',
            })
            .state('signup', {
                url: '/signup',
                templateUrl: 'elements/signup.html',
                controller: 'SignupCtrl',
            })
            .state('logout', {
                url: '/logout',
                template: null,
                controller: 'LogoutCtrl'
            })
            .state('profile', {
                url: '/profile',
                templateUrl: 'elements/profile.html',
                controller: 'ProfileCtrl'
            });
        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise('/');
    }]);